require "#{Alchemy::Engine.root}/app/helpers/alchemy/pages_helper"

module Alchemy
  module PagesHelper
    def render_navigation(options = {}, html_options = {})
      options = {
        submenu: false,
        all_sub_menues: false,
        from_page: @root_page || Language.current_root_page,
        spacer: nil,
        navigation_partial: 'alchemy/navigation/renderer',
        navigation_link_partial: 'alchemy/navigation/link',
        show_nonactive: false,
        restricted_only: false,
        show_title: true,
        reverse: false,
        reverse_children: false
      }.merge(options)
      page = page_or_find(options[:from_page])
      return nil if page.blank?
      pages = page.children#.accessible_by(current_ability, :see)
      pages = pages.restricted if options.delete(:restricted_only)
      if depth = options[:deepness]
        pages = pages.where("#{Page.table_name}.depth <= #{depth}")
      end
      if options[:reverse]
        pages.reverse!
      end
      array = pages.to_a.unshift(page)
      render options[:navigation_partial],
        options: options,
        pages: array,
        html_options: html_options
    end
  end
end
