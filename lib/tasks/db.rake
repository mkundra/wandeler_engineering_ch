namespace :db do

  desc 'Remigrate database and seed data'
  task remigrate: ['db:drop', 'db:create', 'db:migrate', 'alchemy:db:seed'] do
  end

end
