# Pull DB
dropdb wandeler_development && heroku pg:pull DATABASE wandeler_development -a wandeler

# Push DB
heroku pgbackups:capture
heroku pg:reset DATABASE -a wandeler && heroku pg:push wandeler_development DATABASE -a wandeler
